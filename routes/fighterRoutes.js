const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      const fighters = FighterService.getAll();
      res.data = fighters;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    try {
      const fighter = FighterService.search(['id', req.params.id]);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  createFighterValid,
  (req, res, next) => {
    try {
      const fighter = FighterService.createFighter(req.body);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
    }
    next();
  },
  responseMiddleware,
  updateFighterValid,

  (req, res, next) => {
    try {
      const fighterUpToDate = FighterService.update(req.params.id, req.body);
      res.data = fighterUpToDate;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  (req, res, next) => {
    if (Object.keys(req.query).length) {
      res.err = new Error('Bad request');
      next();
    }
    try {
      res.data = FighterService.delete(req.params.id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
