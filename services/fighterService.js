const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  createFighter(fighter) {
    const newFighter = FighterRepository.create(fighter);
    return newFighter;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  update(id, dataToUpdate) {
    if (!this.search(['id', id])) {
      throw new Error('Fighter not exists');
    }
    const fighterUpdated = FighterRepository.update(id, dataToUpdate);
    if (!fighterUpdated) {
      return null;
    }
    return fighterUpdated;
  }

  delete(id) {
    const fighter = FighterRepository.delete(id);
    if (!fighter.length) {
      return null;
    }
    return fighter;
  }
}

module.exports = new FighterService();
