const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  const { id, health, ...requiredFields } = fighter;
  const payload = req.body;
  try {
    if (
      payload.id ||
      !checkContainsOnlyRequiredFields(
        Object.keys(payload),
        Object.keys(requiredFields)
      ) ||
      !checkName(payload.name) ||
      !checkPower(payload.power) ||
      !checkDefense(payload.defense)
    ) {
      throw new Error("Fighter entity to create isn't valid");
    } else {
      req.body.health = fighter.health;
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.toString(),
    });
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const { id, health, ...allowedFields } = fighter;
  const payload = req.body;
  try {
    if (
      !Object.keys(payload).length ||
      payload.id ||
      !checkContainsOnlyAllowedFields(
        Object.keys(payload),
        Object.keys(allowedFields)
      )
    ) {
      throw new Error("Fighter entity to update isn't valid");
    }
    if (payload.name && !checkName(payload.name)) {
      throw new Error("Name isn't valid");
    }
    if (payload.power && !checkPower(payload.power)) {
      throw new Error("Power isn't valid");
    }
    if (payload.defense && !checkDefense) {
      throw new Error("Defense isn't valid");
    } else {
      req.body.health = fighter.health;
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.toString(),
    });
  }
};

function checkContainsOnlyRequiredFields(payloadKeys, requiredKeys) {
  return (
    payloadKeys.length === requiredKeys.length &&
    requiredKeys.every((key) => payloadKeys.includes(key))
  );
}

function checkContainsOnlyAllowedFields(payloadKeys, allowedKeys) {
  return payloadKeys.every((key) => allowedKeys.includes(key));
}

function checkName(name) {
  return Boolean(name.trim());
}

function checkPower(power) {
  return power <= 100 && power > 0 && isFinite(power);
}

function checkDefense(defense) {
  return defense >= 1 && defense <= 10;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
