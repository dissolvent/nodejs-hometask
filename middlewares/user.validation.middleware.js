const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  const { id, ...requiredFields } = user;
  const payload = req.body;
  try {
    if (
      payload.id ||
      !checkContainsOnlyRequiredFields(
        Object.keys(payload),
        Object.keys(requiredFields)
      ) ||
      !checkFirstName(payload.firstName) ||
      !checkLastName(payload.lastName) ||
      !checkEmail(payload.email) ||
      !checkPhoneNumber(payload.phoneNumber) ||
      !checkPassword(payload.password)
    ) {
      throw new Error("User entity to create isn't valid");
    } else {
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.toString(),
    });
  }
};

const updateUserValid = (req, res, next) => {
  const { id, ...allowedFields } = user;
  const payload = req.body;
  try {
    if (
      !Object.keys(payload).length ||
      payload.id ||
      !checkContainsOnlyAllowedFields(
        Object.keys(payload),
        Object.keys(allowedFields)
      )
    ) {
      throw new Error("User entity to update isn't valid");
    }
    if (payload.firstName && !checkFirstName(payload.firstName)) {
      throw new Error("Firstname isn't valid");
    }
    if (payload.lastName && !checkLastName(payload.lastName)) {
      throw new Error("Lastname isn't valid");
    }
    if (payload.email && !checkEmail(payload.email)) {
      throw new Error("Email isn't valid");
    }
    if (payload.phoneNumber && !checkPhoneNumber(payload.phoneNumber)) {
      throw new Error("Phone number isn't valid");
    }
    if (payload.password && !checkPassword(payload.password)) {
      throw new Error("Password isn't valid");
    } else {
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: err.toString(),
    });
  }
};

function checkContainsOnlyRequiredFields(payloadKeys, requiredKeys) {
  return (
    payloadKeys.length === requiredKeys.length &&
    requiredKeys.every((key) => payloadKeys.includes(key))
  );
}

function checkContainsOnlyAllowedFields(payloadKeys, allowedKeys) {
  return payloadKeys.every((key) => allowedKeys.includes(key));
}

function checkFirstName(firstName) {
  return Boolean(firstName.trim());
}

function checkLastName(lastName) {
  return Boolean(lastName.trim());
}

function checkEmail(email) {
  return (
    email.trim().endsWith('@gmail.com') &&
    email.trim().length - '@gmail.com'.length > 0
  );
}

function checkPhoneNumber(phoneNumber) {
  return (
    phoneNumber.trim().startsWith('+380') &&
    phoneNumber.trim().length === 13 &&
    new RegExp('^\\d+$', 'g').test(phoneNumber.substring(4, phoneNumber.length))
  );
}

function checkPassword(password) {
  return password.trim().length >= 3;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
